package com.yasser.shedlock.repository;

import com.yasser.shedlock.domain.ScheduledTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScheduledTestRepository extends JpaRepository<ScheduledTest, Long> {
}