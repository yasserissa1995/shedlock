package com.yasser.shedlock.domain;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "SHEDLOCK")
@Getter
@Setter
@EqualsAndHashCode
public class ShedLock implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "name")
    private String name;

    @Column(name = "lock_until")
    @NotNull
    private LocalDateTime lockUntil;

    @Column(name = "locked_at")
    @NotNull
    private LocalDateTime lockedAt = LocalDateTime.now();

    @Column(name = "locked_by")
    @NotNull
    private String lockedBy;


}
