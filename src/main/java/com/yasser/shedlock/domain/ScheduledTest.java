package com.yasser.shedlock.domain;

import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "scheduled_test")
@Getter
@Setter
@EqualsAndHashCode
public class ScheduledTest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;


    @Column(name = "curr_time")
    private LocalDateTime currTime = LocalDateTime.now();

    @Column(name = "locked_by")
    private String lockedBy;


}

