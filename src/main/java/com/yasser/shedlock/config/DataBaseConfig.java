package com.yasser.shedlock.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackages = { "com.yasser.shedlock.repository" })
@EnableTransactionManagement
public class DataBaseConfig {
}
