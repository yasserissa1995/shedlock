package com.yasser.shedlock.service;

import com.yasser.shedlock.domain.ScheduledTest;
import com.yasser.shedlock.repository.ScheduledTestRepository;
import jakarta.transaction.Transactional;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ScheduledTestService {
    private final Logger log = LoggerFactory.getLogger(ScheduledTestService.class);

    @Value("${spring.application.name}")
    private String appName;

    @Value("${server.port}")
    private String appPort;

    private final ScheduledTestRepository scheduledTestRepository;

    public ScheduledTestService(ScheduledTestRepository scheduledTestRepository) {
        this.scheduledTestRepository = scheduledTestRepository;
    }

    @Scheduled(cron = "0 * * * * *")
    @SchedulerLock(name = "yy")
    public void run() throws InterruptedException {

        ScheduledTest scheduledTest = new ScheduledTest();
        scheduledTest.setLockedBy(appName + " : " + appPort);
        scheduledTestRepository.save(scheduledTest);

        log.debug("done;");
    }
}
